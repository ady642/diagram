import Point from '@/models/Point'
import { v4 as uuidv4 } from 'uuid'
import Shape from '@/models/Shape'

type Optional<T, K extends keyof T> = Pick<Partial<T>, K> & Omit<T, K>;

export default class Circle {
  id: string;
  position: Point;
  relatedShape: Shape;
  isSelected?: boolean;

  constructor ({ id, position, isSelected, relatedShape }: Optional<Circle, 'id'>) {
    this.id = id ?? uuidv4()
    this.position = position
    this.isSelected = isSelected
    this.relatedShape = relatedShape
  }
}
