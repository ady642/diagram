import Point from '@/models/Point'
import Line from '@/models/Line'
import { v4 as uuidv4 } from 'uuid'
import Circle from '@/models/Circle'

export default class Shape {
  id: string;
  position: Point;
  lines?: Line[];
  isMoving?: boolean;
  isSelected?: boolean;
  width?: number;
  height?: number;
  attachedCircles?: Circle[]

  constructor () {
    this.id = uuidv4()
    this.height = 130
    this.width = 130
    this.position = new Point(200, 200)
    this.lines = []
    this.isMoving = false
    this.isSelected = false
    this.attachedCircles = []
  }

  calculateCircles (): void {
    const width = this.width ?? 0
    const height = this.height ?? 0

    const circle1Position = new Point(this.position.x + width / 2, this.position.y)
    const circle2Position = new Point(this.position.x + width / 2, this.position.y + height)
    const circle3Position = new Point(this.position.x + width, this.position.y + height / 2)
    const circle4Position = new Point(this.position.x, this.position.y + height / 2)

    if (this.attachedCircles && this.attachedCircles.length > 0) { // If this.attachedCircles already exists, just change the position and keep the ids
      this.attachedCircles[0].position = circle1Position
      this.attachedCircles[1].position = circle2Position
      this.attachedCircles[2].position = circle3Position
      this.attachedCircles[3].position = circle4Position
    } else {
      this.attachedCircles = [
        new Circle({
          position: circle1Position,
          relatedShape: this
        }),
        new Circle({
          position: circle2Position,
          relatedShape: this
        }),
        new Circle({
          position: circle3Position,
          relatedShape: this
        }),
        new Circle({
          position: circle4Position,
          relatedShape: this
        })
      ]
    }
  }
}
