import Shape from '@/models/Shape'
import { v4 as uuidv4 } from 'uuid'
import Circle from '@/models/Circle'

type lineAttachment = { shape: Shape, circle: Circle }

export default class Line {
  id: string;
  source?: lineAttachment;
  target?: lineAttachment;

  constructor () {
    this.id = uuidv4()
  }

  hasSource (): boolean {
    return !!this.source
  }

  hasTarget (): boolean {
    return !!this.target
  }

  setSource (lineAttachment: lineAttachment): void {
    this.source = lineAttachment
  }

  setTarget (lineAttachment: lineAttachment): void {
    this.target = lineAttachment
  }
}
