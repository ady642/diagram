import { reactive, Ref } from 'vue'
import Shape from '@/models/Shape'
import Point from '@/models/Point'
import Line from '@/models/Line'

type stateType = {
  shapesCollection: Set<Shape>;
  linesCollection: Set<Line>;
  selectedShape: Shape;
};

const useMovement = (svgContainerRef: Ref<HTMLElement>, state: stateType) => {
  const movingState = reactive({
    isMoving: false,
    deltaToCentered: new Point(0, 0)
  })

  const convertToSVGContainerCoords = (
    svgContainer: HTMLElement,
    e: PointerEvent
  ) => {
    const svgContainerBox = svgContainer.getBoundingClientRect()

    return {
      x: e.clientX - svgContainerBox.x,
      y: e.clientY - svgContainerBox.y
    }
  }

  const onMouseDown = (shape: Shape, event: PointerEvent) => {
    movingState.isMoving = true
    state.selectedShape = shape

    const { x, y } = convertToSVGContainerCoords(svgContainerRef.value, event)

    movingState.deltaToCentered = new Point(
      x - shape.position.x,
      y - shape.position.y
    )
  }

  const onMouseMove = (event: PointerEvent) => {
    // onMouseMove must be placed on the svg container to cover the case when the mouse move fast and go out of the shape
    if (movingState.isMoving === false) {
      return
    }

    const { x, y } = convertToSVGContainerCoords(svgContainerRef.value, event)

    state.selectedShape.position = new Point(
      x - movingState.deltaToCentered.x,
      y - movingState.deltaToCentered.y
    )
    state.selectedShape.isMoving = true
  }

  const onMouseUp = () => {
    movingState.isMoving = false
    state.selectedShape.isMoving = false
  }

  return {
    onMouseDown,
    onMouseMove,
    onMouseUp
  }
}

export default useMovement
